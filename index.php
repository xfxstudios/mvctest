<?php

class Index 
{
    private $default = "Inicio";

    public function construct(){
        
    }
    //Carga el Controlador por defecto si no se especifica uno por url
    public function control($X=null){
        if($X==null){
            //Cargo el controlador por defecto
            include('controller/'.$this->default.'.php');
            $class = new $this->default;
            return $class->index();
        }else{
            //Cargo el controlador solicitado
            include('controller/'.$X.'.php');
            $class = new $X;
            return $class->index();
        }
    }
}

//Inicia la clase y verifica si se esta solicitando un controlador por url en caso contrario carga uno por defecto
$carga = new Index();
if(isset($_GET['r'])){
    //Cargamos el Controlador solicitado por url
    $carga->control($_GET['r']);
}else{
    //Cargamos el Controlador por defecto
    $carga->control();
}


?>