<?php
class MV_Controller
{
    public function _construct(){

    }
    //Carga las vistas solicitadas
    public function view($X){
        if(is_array($X)){
            $pag = "";
            foreach($X as $item){
                $pag .= include('view/'.$item.'.php');
            }
            return $pag;
        }else{
            return include('view/'.$X.'.php');
        }
    }


    //Carga los modelos solicitados
    public function model($X){
        if(is_array($X)){
            $pag = "";
            foreach($X as $item){
                $pag .= include('model/'.$item.'.php');
            }
            return $pag;
        }else{
            return include('model/'.$X.'.php');
        }
    }
}

?>