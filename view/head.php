<!doctype html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <title>Muestra MVC</title>

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


        <!-- Custom font from Google Web Fonts -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400" rel="stylesheet">

        <!-- Social font -->
        <link href="assets/css/social-font.css" rel="stylesheet">

        <!-- Template stylesheet -->
        <link href="assets/css/metronome.css" rel="stylesheet">

        <!-- Demo stylesheet -->
        <link href="assets/css/demo.css" rel="stylesheet">
    </head>

    <body>